import java.awt.*;

public class NullReference{
	private static Rectangle rectangle;
	public static void main(String[]args){
		int area;
		if(rectangle == null){
			System.out.println("rectangle variable doesn't refer to a Rectangle objct");
		}else{
			area = rectangle.area();
			System.out.println("Area:"+area);
		}
	}
}
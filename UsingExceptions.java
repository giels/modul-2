import java.util.*;

public class UsingExceptions{
	public static void main(String []args){
		try{
			throwException();
		}catch (Exception e){
			System.err.println("Exception handlede in main");
		}
		doesNotThrowException();
	}

	public static void throwException() throws Exception{
		try{
			System.out.println("MEthod throw Exception");
			throw new Exception();
		}catch (Exception e){
			System.err.println("Exception handled in MEthod throwException");
			throw new Exception();
		}finally{
			System.out.println("Finally executed in ThrowException");
		}
	}

		public static void doesNotThrowException(){
		try{
			System.out.println("MEthod doesNotThrow Exception");
		}catch (Exception e){
			System.err.println(e);
		}finally{
			System.out.println("Finally executed in doesNotThrowException");
		}
		System.out.println("End of method doesNotThrowException");
	}
}
import java.util.Scanner;
import java.util.InputMismatchException;

public class DivideByZeroWithExceptionHandling{
	public static int pembagian(int bil, int pbg) throws ArithmeticException{
		return bil / pbg;
	}
	public static void main(String []args){
		Scanner scanner = new Scanner(System.in);

		boolean continueLoop = true;

		do{
			try{
				System.out.print("Please enter an integer numenator:");
				int numenator = scanner.nextInt();
				System.out.print("Please enter an integer denominator:");
				int denominator = scanner.nextInt();

				int result = pembagian(numenator, denominator);
				System.out.printf("%n Result: %d / %d = %d%n", numenator,denominator,result);
				continueLoop = false;
			}
			catch(InputMismatchException e){
				System.err.printf("%nException :%s%n", e);
				scanner.nextLine();
				System.out.printf("You must enter integer. please try again. %n%n");
			}
			catch(ArithmeticException e){
				System.err.printf("%nException :%s%n", e);
				System.out.printf("Zero is invalid denominator. please try again %n%n");
			}
		}while(continueLoop);
	}
}
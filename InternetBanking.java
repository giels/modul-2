public class InternetBanking {
	public static void main(String[] args){
	Account xAcc = new Account();
	xAcc.credit(1000);
	xAcc.debit(2000);
	Account yAcc = new Account();
	yAcc.credit(1200);
	yAcc.debit(2000);
	FundTransfer.transferFunds(xAcc, yAcc, 1400);
	System.out.println("xAcc's current balance " + xAcc.getCurrentBalance());
	System.out.println("yAcc's current balance " + yAcc.getCurrentBalance());
	System.out.println("Completed execution of main method");
	}
}
import java.util.Scanner;

public class DivideByZeroNoExceptionHandling{
	public static int pembagian(int bil, int pbg){
		return bil/pbg;
	}
	public static void main(String[]args){
		Scanner scanner = new Scanner(System.in);

		System.out.print("Masukkan nilai pembilang:");
		int numerator = scanner.nextInt();
		System.out.print("Masukkan nilai pembilang(pembagi):");
		int denominator = scanner.nextInt();

		int result = pembagian (numerator,denominator);
		System.out.printf("%nresult: %d / %d = %d %n", numerator, denominator,result);
	}
}